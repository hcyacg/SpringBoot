package com.sy.service;

import com.sy.entity.Dept;

import java.util.List;

public interface DeptService {
    List<Dept> getAllDepts();
}
