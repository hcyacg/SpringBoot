package com.sy.mapper;

import com.sy.entity.Dept;

import java.util.List;

public interface DeptMapper {
    List<Dept> getAllDepts();
}
