package com.sy.controller;

import com.sy.entity.Dept;
import com.sy.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DeptController {
    @Autowired
    private DeptService deptService;

    @RequestMapping(value = "depts", method = RequestMethod.GET)
    public List<Dept> getAllDepts() {
        return deptService.getAllDepts();
    }
}
